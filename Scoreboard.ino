#include <FastLED.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#define NUM_LEDS 256     // Total number of LEDs in your matrix
#define DATA_PIN 2       // The pin connected to the data-in of the LED strip
#define WIDTH 32         // Width of the LED matrix
#define HEIGHT 8         // Height of the LED matrix
#define DIGIT_WIDTH 3    // Width of a single digit
#define DIGIT_HEIGHT 5   // Height of a single digit
#define TEAM_ONE_BUTTON_PIN 5  // GPIO pin for Team One's button
#define TEAM_TWO_BUTTON_PIN 4  // GPIO pin for Team Two's button
#define TIMER_CONTROL_BUTTON_PIN 14  // GPIO pin for Timer Control button
#define TIMER_INCREMENT_BUTTON_PIN 12  // GPIO pin for Timer Increment button

// Global variable declarations
int teamOneScore = 0;         // Variable to track Team One's score
int teamTwoScore = 0;         // Variable to track Team Two's score
unsigned long lastButtonPress = 0;  // Time of the last button press
unsigned long timerStart = 0;       // Start time for the timer
bool timerPaused = false;
unsigned long lastTimerControlPress = 0; // Time of the last timer control button press
unsigned long totalTimeSet = 0; // Total time set for the timer in milliseconds
unsigned long elapsedBeforePause = 0; // Variable to store elapsed time before pause
unsigned long timerIncrementButtonPressStartTime = 0; // Time when the button press started
bool timerIncrementButtonLongPressDetected = false; // Flag to indicate a long press

ESP8266WebServer server(80);

const char* ssid = "Cardiff Unicycle Hockey";
const char* password = "UnicycleHockey"; // Choose a password for AP mode

String getHTML() {
    return R"=====(
    <html>
    <head>
        <title>Cardiff Unicycle Hockey Scoreboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            body {
                font-family: 'Arial', sans-serif;
                text-align: center;
                margin: 0;
                padding: 0;
                background-color: #333;
                color: #FFFFFF;
            }
            .scoreboard {
                display: flex;
                justify-content: space-around;
                margin: 20px auto;
                padding: 10px;
                background: rgba(0, 0, 0, 0.7);
                border-radius: 10px;
                width: 95%;
                max-width: 600px;
            }
            .team-score, .time-display {
                text-align: center;
            }
            .score, .time {
                font-size: 48px;
                font-weight: bold;
            }
            .label {
                font-size: 24px;
                margin-top: 10px;
            }
            .button {
                background-color: #007BFF; /* Blue */
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 5px;
                width: 80%;
                max-width: 300px;
            }
            select {
                padding: 10px;
                font-size: 16px;
                margin: 10px;
                width: 60%;
                max-width: 300px;
                border-radius: 5px;
                background-color: white;
                color: black;
            }
        </style>
    </head>
    <body>
        <h1>Cardiff Unicycle Hockey</h1>
        <div class="scoreboard">
            <div class="team-score">
                <div class="label">Team 1</div>
                <div id="teamOneScore" class="score">0</div>
            </div>
            <div class="team-score">
                <div class="label">Team 2</div>
                <div id="teamTwoScore" class="score">0</div>
            </div>
        </div>
        <div class="time-display">
            <div class="label">Time</div>
            <div id="timer" class="time">00:00</div>
        </div>
        <button class="button" onclick="sendCommand('incrementTeamOne')">Team 1</button>
        <button class="button" onclick="sendCommand('incrementTeamTwo')">Team 2</button>
        <button class="button" onclick="sendCommand('toggleTimer')">Start / Stop Timer</button>
        <select id="timeSelect" class="button">
            <option value="01:00">1:00</option>
            <option value="02:00">2:00</option>
            <!-- Add options up to 15:00 -->
            <option value="15:00">15:00</option>
        </select>
        <button class="button" onclick="setTime()">Set Time</button>
        <button class="button" onclick="sendCommand('reset')">Reset Scoreboard</button>

        <script>
            function sendCommand(command) {
                fetch('/' + command).then(response => response.text()).then(data => {
                    var status = data.split(',');
                    document.getElementById('teamOneScore').innerHTML = status[0].split(':')[1].trim();
                    document.getElementById('teamTwoScore').innerHTML = status[1].trim().split(':')[1].trim();
                    document.getElementById('timer').innerHTML = status[2].trim().split(':')[1].trim();
                });
            }

            function setTime() {
                var time = document.getElementById('timeSelect').value;
                fetch('/setTime?time=' + time).then(response => response.text()).then(data => {
                    var status = data.split(',');
                    document.getElementById('timer').innerHTML = status[2].trim().split(':')[1].trim();
                });
            }

            function updateStatus() {
                fetch('/status').then(response => response.text()).then(data => {
                    var status = data.split(',');
                    document.getElementById('teamOneScore').innerHTML = status[0].split(':')[1].trim();
                    document.getElementById('teamTwoScore').innerHTML = status[1].trim().split(':')[1].trim();
                    document.getElementById('timer').innerHTML = status[2].trim().split(':')[1].trim();
                });
            }

            setInterval(updateStatus, 1000); // Update the status every second
        </script>
    </body>
    </html>
    )=====";
}

CRGB leds[NUM_LEDS]; // Array to hold LED data

// 3D array to define the patterns for digits 0-9 and the dash
byte patterns[11][DIGIT_HEIGHT][DIGIT_WIDTH] = {
  
    // 0
    {
    {1, 1, 1}, 
    {1, 0, 1}, 
    {1, 0, 1}, 
    {1, 0, 1}, 
    {1, 1, 1}
    },
    
    // 1
     {
     {0, 1, 0}, 
     {1, 1, 0}, 
     {0, 1, 0}, 
     {0, 1, 0}, 
     {1, 1, 1}
     },
     
    // 2
      {
      {1, 1, 1}, 
      {0, 0, 1}, 
      {1, 1, 1}, 
      {1, 0, 0}, 
      {1, 1, 1}
      },
      
    // 3
      {
      {1, 1, 1}, 
      {0, 0, 1}, 
      {1, 1, 1}, 
      {0, 0, 1}, 
      {1, 1, 1}
      },

    // 4
      {
      {1, 0, 1}, 
      {1, 0, 1}, 
      {1, 1, 1}, 
      {0, 0, 1}, 
      {0, 0, 1}
      },

     // 5
      {
      {1, 1, 1}, 
      {1, 0, 0}, 
      {1, 1, 1}, 
      {0, 0, 1}, 
      {1, 1, 1}
      },

     // 6
      {
      {1, 1, 1}, 
      {1, 0, 0}, 
      {1, 1, 1}, 
      {1, 0, 1}, 
      {1, 1, 1}
      },

     // 7
      {
      {1, 1, 1}, 
      {0, 0, 1}, 
      {0, 0, 1}, 
      {0, 0, 1}, 
      {0, 0, 1}
      },

     // 8
      {
      {1, 1, 1}, 
      {1, 0, 1}, 
      {1, 1, 1}, 
      {1, 0, 1}, 
      {1, 1, 1}
      },
    
    // 9
      {
      {1, 1, 1}, 
      {1, 0, 1}, 
      {1, 1, 1}, 
      {0, 0, 1}, 
      {1, 1, 1}
      },

};
    // Dash (-)
     // {
     // {0, 0, 0}, 
     // {0, 0, 0}, 
     // {1, 1, 1}, 
     // {0, 0, 0}, 
     // {0, 0, 0}
     // },

     // Colon pattern
byte colonPattern[DIGIT_HEIGHT] = {0, 1, 0, 1, 0};

void displayScores();
void clearMatrix();
void displayDigit(int num, int x_offset, int y_offset);
void displayTimer(int minutes, int seconds);
String getHTML();

void setup() {
    Serial.begin(115200);
    WiFi.softAP(ssid, password);
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);

    server.on("/", HTTP_GET, []() {
        server.send(200, "text/html", getHTML());
    });

    server.on("/incrementTeamOne", HTTP_GET, []() {
        teamOneScore = (teamOneScore + 1) % 100;
        displayScores();
        server.send(200, "text/plain", "Team One Score Incremented");
    });

    server.on("/incrementTeamTwo", HTTP_GET, []() {
        teamTwoScore = (teamTwoScore + 1) % 100;
        displayScores();
        server.send(200, "text/plain", "Team Two Score Incremented");
    });

   server.on("/setTime", HTTP_GET, []() {
    if (!server.hasArg("time")) {
        server.send(404, "text/plain", "Time not specified");
        return;
    }
    String time = server.arg("time"); // Using 'time' as a query parameter
    int minutes = time.substring(0, time.indexOf(':')).toInt();
    int seconds = time.substring(time.indexOf(':') + 1).toInt();
    totalTimeSet = (minutes * 60 + seconds) * 1000;

    // Always clear the matrix and display the new time
    clearMatrix();
    int displayMinutes = totalTimeSet / 60000;
    int displaySeconds = (totalTimeSet % 60000) / 1000;
    displayTimer(displayMinutes, displaySeconds);
    FastLED.show();

    // Conditionally adjust timerStart if the timer was active
    if (timerStart != 0 && !timerPaused) {
        // If you want to restart the timer with the new time immediately
        timerStart = millis();
        // Note: This will reset any elapsed time. Remove this line if you wish to adjust the timer without restarting.
    }

    server.send(200, "text/plain", "Time Set to " + time);
});

    server.on("/toggleTimer", HTTP_GET, []() {
    if (!timerPaused && timerStart == 0 && totalTimeSet > 0) { // Check if totalTimeSet is greater than 0
        timerStart = millis();
        timerPaused = false;
    } else if (!timerPaused && timerStart != 0) {
        timerPaused = true;
        elapsedBeforePause = millis() - timerStart;
    } else {
        timerStart = millis() - elapsedBeforePause;
        timerPaused = false;
    }
    // This sends the timer's current state back to the webpage, you might want to adjust this response
    server.send(200, "text/plain", "Timer Toggled");
});

   server.on("/reset", HTTP_GET, []() {
    teamOneScore = 0; // Reset Team One's score
    teamTwoScore = 0; // Reset Team Two's score
    timerStart = 0; // Clear timer start
    totalTimeSet = 0; // Reset total time set for the timer
    timerPaused = false; // Ensure timer is not paused
    elapsedBeforePause = 0; // Clear any elapsed time before pause
    
    clearMatrix(); // Clear the LED matrix display
    
    // Redraw the timer as 00:00 to indicate reset
    displayTimer(0, 0);
    
    // Redraw scores (which would be 0:0 after reset)
    displayScores();
    
    FastLED.show(); // Update the LED matrix with the changes
    
    server.send(200, "text/plain", "Scoreboard Reset"); // Send a confirmation response
});

    server.on("/status", HTTP_GET, []() {
    unsigned long remainingTime = 0;
    if(timerStart > 0 && !timerPaused) {
        unsigned long elapsedTime = millis() - timerStart;
        if(elapsedTime < totalTimeSet) {
            remainingTime = totalTimeSet - elapsedTime;
        }
    } else if (timerPaused) {
        remainingTime = totalTimeSet - elapsedBeforePause;
    } else {
        remainingTime = totalTimeSet;
    }
    int remainingSeconds = (remainingTime / 1000) % 60;
    int remainingMinutes = (remainingTime / 60000);
    String status = "Team 1: " + String(teamOneScore) + ", Team 2: " + String(teamTwoScore) + ", Time: " + String(remainingMinutes) + ":" + (remainingSeconds < 10 ? "0" : "") + String(remainingSeconds);
    server.send(200, "text/plain", status);
});

    server.begin();
    Serial.println("HTTP server started");

    FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
    FastLED.setBrightness(50);

    pinMode(TEAM_ONE_BUTTON_PIN, INPUT_PULLUP);
    pinMode(TEAM_TWO_BUTTON_PIN, INPUT_PULLUP);
    pinMode(TIMER_CONTROL_BUTTON_PIN, INPUT_PULLUP);
    pinMode(TIMER_INCREMENT_BUTTON_PIN, INPUT_PULLUP);

    // Display initial timer and scores on power up
    clearMatrix();
    displayTimer(0, 0);  // Show initial timer as 00:00
    displayScores();     // Show initial team scores
    FastLED.show();
}

int getLedIndex(int x, int y) {
    int i = (x % 2 == 0) ? (x * HEIGHT + y) : ((x + 1) * HEIGHT - y - 1);
    return i;
}

void clearMatrix() {
    fill_solid(leds, NUM_LEDS, CRGB::Black);
}

void displayDigit(int num, int x_offset, int y_offset) {
    for (int row = 0; row < DIGIT_HEIGHT; row++) {
        for (int col = 0; col < DIGIT_WIDTH; col++) {
            int index = getLedIndex(x_offset + col, y_offset + row);
            leds[index] = patterns[num][row][col] ? CRGB::Blue : CRGB::Black;
        }
    }
}

void displayTimer(int minutes, int seconds) {
    int timerOffset = (WIDTH - 4 * DIGIT_WIDTH - 1) / 2 - 3; // adjusted to move timer 3 pixels to the left
    displayDigit(minutes / 10, timerOffset, 0); // First digit of minutes
    displayDigit(minutes % 10, timerOffset + DIGIT_WIDTH, 0); // Second digit of minutes

    // Display colon between minutes and seconds
    for (int row = 0; row < DIGIT_HEIGHT; row++) {
        leds[getLedIndex(timerOffset + 2 * DIGIT_WIDTH, row)] = colonPattern[row] ? CRGB::Green : CRGB::Black;
    }

    displayDigit(seconds / 10, timerOffset + 2 * DIGIT_WIDTH + 1, 0); // First digit of seconds
    displayDigit(seconds % 10, timerOffset + 3 * DIGIT_WIDTH + 1, 0); // Second digit of seconds
}

void displayScores() {
    // Display Team One's score at the very left edge
    displayDigit(teamOneScore / 10, 0, 3);
    displayDigit(teamOneScore % 10, DIGIT_WIDTH, 3);

    // Display Team Two's score at the very right edge
    int teamTwoOffset = WIDTH - 2 * DIGIT_WIDTH -7; // Moves the time score 6 pixels to the left from the edge 
    displayDigit(teamTwoScore / 10, teamTwoOffset, 3);
    displayDigit(teamTwoScore % 10, teamTwoOffset + DIGIT_WIDTH, 3);
}

void loop() {
  server.handleClient();

    unsigned long currentTime = millis();

    // Team One button logic
    if (digitalRead(TEAM_ONE_BUTTON_PIN) == LOW) {
        if (currentTime - lastButtonPress > 500) { // Increased debounce time
            lastButtonPress = currentTime;
            teamOneScore = (teamOneScore + 1) % 21; // Score up to 20
            displayScores();
        }
    }

    // Team Two button logic
    if (digitalRead(TEAM_TWO_BUTTON_PIN) == LOW) {
        if (currentTime - lastButtonPress > 500) { // Increased debounce time
            lastButtonPress = currentTime;
            teamTwoScore = (teamTwoScore + 1) % 21; // Score up to 20
            displayScores();
        }
    }

    // Timer Increment and Reset button logic
if (digitalRead(TIMER_INCREMENT_BUTTON_PIN) == LOW) {
    if (timerIncrementButtonPressStartTime == 0) { // Button press detected
        timerIncrementButtonPressStartTime = currentTime;
    } else if ((currentTime - timerIncrementButtonPressStartTime) > 3000 && !timerIncrementButtonLongPressDetected) {
        // Long press detected, perform reset
        timerIncrementButtonLongPressDetected = true; // Prevent multiple resets
        // Reset logic
        teamOneScore = 0;
        teamTwoScore = 0;
        timerStart = 0;
        totalTimeSet = 0;
        timerPaused = false;
        elapsedBeforePause = 0;
        clearMatrix();
        displayTimer(0, 0); // Show initial timer as 00:00
        displayScores(); // Show initial team scores
    }
} else {
    if (timerIncrementButtonPressStartTime != 0 && !timerIncrementButtonLongPressDetected) {
        // Short press logic, add 1 minute to total time
        totalTimeSet += 60000; // Add 1 minute to total time set
        // Display the incremented time immediately
        unsigned long displayTime = totalTimeSet;
        if(timerStart == 0 || timerPaused) { // Only update the display if the timer isn't actively counting down
            int displaySeconds = (displayTime / 1000) % 60;
            int displayMinutes = (displayTime / 60000);
            clearMatrix();
            displayTimer(displayMinutes, displaySeconds); // Show updated time
        }
    }

// Timer Control button logic for start/pause/resume
if (digitalRead(TIMER_CONTROL_BUTTON_PIN) == LOW && currentTime - lastTimerControlPress > 500) {
    lastTimerControlPress = currentTime;
    if (!timerPaused && timerStart == 0 && totalTimeSet > 0) {
        // Start the timer
        timerStart = currentTime;
        timerPaused = false;
    } else if (!timerPaused && timerStart != 0) {
        // Pause the timer
        timerPaused = true;
        elapsedBeforePause = currentTime - timerStart; // Save elapsed time before pause
    } else if (timerPaused) {
        // Resume the timer
        timerPaused = false;
        timerStart = currentTime - elapsedBeforePause; // Adjust start time for the remaining time
    }
}

    // Reset variables after button release
    timerIncrementButtonPressStartTime = 0;
    timerIncrementButtonLongPressDetected = false;
}

    // Timer display logic for countdown
if (timerStart > 0 && !timerPaused) {
    unsigned long elapsedTime = currentTime - timerStart;
    if (elapsedTime < totalTimeSet) {
        unsigned long remainingTime = totalTimeSet - elapsedTime;
        int remainingSeconds = (remainingTime / 1000) % 60;
        int remainingMinutes = (remainingTime / 60000);
        clearMatrix();
        displayTimer(remainingMinutes, remainingSeconds); // Display countdown
    } else {
        // When countdown reaches zero, flash panel red 10 times
        for (int i = 0; i < 10; i++) {
            fill_solid(leds, NUM_LEDS, CRGB::Red); // Turn all LEDs red
            FastLED.show();
            delay(250); // Delay to keep LEDs red
            fill_solid(leds, NUM_LEDS, CRGB::Black); // Turn all LEDs off
            FastLED.show();
            delay(250); // Delay before the next flash
        }

        // Reset timer when countdown completes
        timerStart = 0;
        totalTimeSet = 0;
        timerPaused = false;
        clearMatrix();
        displayTimer(0, 0); // Display reset timer
        displayScores(); // Refresh scores display after flashing
    }
}

    displayScores(); // Ensure scores are displayed outside the if block to avoid being overwritten by timer display
    FastLED.show();
    delay(10); // Small delay to prevent button bounce
}

