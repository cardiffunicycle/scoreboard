Arduino Scoreboard Project

## Description
This Arduino project creates a digital scoreboard, suitable for tracking scores in games. It uses an LED matrix to display scores for two teams and includes buttons for score adjustments and timer control..

## Key Features
- **Dynamic LED Scoring Display**: Utilizes a 32x8 LED matrix for clear and bright score presentation.
- **Interactive Game Timer**: Includes start, pause, and reset functionalities to keep track of game time.
- **Easy-to-Use Score Adjustment**: Scores for two teams can be adjusted with simple button presses.
- **Expandability for Remote Control**: Prepared groundwork for future Wi-Fi connectivity for score adjustments and timer control via a web interface.

## Visuals

## Installation

### Prerequisites
Before starting, ensure you have the following:
- An Arduino IDE installed on your computer.
- The `FastLED`, `ESP8266WiFi`, and `ESP8266WebServer` libraries installed through the Arduino IDE's Library Manager.

### Hardware Setup
1. **ESP8266 Board**: Use any compatible ESP8266 board.
2. **LED Matrix**: A 32x8 LED matrix connected to the ESP8266 through DATA_PIN 2.
3. **Buttons**: Connect four buttons for Team One, Team Two, Timer Control, and Timer Increment to GPIO pins 5, 4, 14, and 12, respectively.

### Software Configuration
1. Open the `Scoreboard.ino` file in the Arduino IDE.
2. Adjust the `#define` statements at the top of the script if your hardware differs from the default setup.
3. Upload the code to your ESP8266 board.

## Usage
### Starting the Scoreboard
Power on the ESP8266 board. The LED matrix lights up, indicating the scoreboard is ready.

### Adjusting Scores
- **Increment Score**: Press the Team One or Team Two button to increment their respective scores.
- **Reset Score**: Hold reset button for 3 seconds to reset scores & timer to zero.

### Managing the Timer
- **Start/Stop Timer**: Press the Timer Control button to start or stop the timer.
- **Increment Timer**: Press the Timer Increment button to add 1 minute to the timer.

## Support

## Roadmap
- Add audio such as a buzzer.
- Minor design changes for ease of assemble and disassembly. 
- Code changes to allow colour changes of scores and timer.
- New power supply, currectly run on usb power bank through USB-C, would like to switch to 18650 cells. 
- PCB design for cleaner, simple assembly
- Light sensor to control brightness. 

## Contributing
Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

## Authors and acknowledgment

## License

## Project status
On going development 
